Document all concepts and your implementation decisions.


#Flow of the program
The program starts with `app.init()`. If you want to choose between the various mode there are to get data in your application you need to comment out the other functions and calls.

TODO: Improve this flow
1. Make selection easier which way to retrieve data



#Concepts
For every concept the following items:
- short description
- code example
- reference to mandatory documentation
- reference to alternative documentation (authoritive and authentic)

###Objects, including object creation and inheritance

#####JavaScript objects:

This code assigns a simple value (Fiat) to a variable named car:

    var car = "Fiat";

Objects are variables too. But objects can contain many values.

This code assigns many values (Fiat, 500, white) to a variable named car:

    var car = {type:"Fiat", model:500, color:"white"};

You can also have paired values like this:

    var person = {firstName:"John", lastName:"Doe", age:50, eyeColor:"blue"};

source: http://www.w3schools.com/js/js_objects.asp

####Object-oriented programming
With JavaScript you can do object-oriented programming because JavaScript is object-oriented to its core, with powerful, flexible OOP capabilities.
This goes like the following:

    // Define the Person constructor
    var Person = function(firstName) {
      this.firstName = firstName;
    };


    // Add a couple of methods to Person.prototype
    Person.prototype.walk = function(){
      console.log("I am walking!");
    };

    Person.prototype.sayHello = function(){
      console.log("Hello, I'm " + this.firstName);
    };

    // Define the Student constructor
    function Student(firstName, subject) {
      // Call the parent constructor, making sure (using Function#call)
      // that "this" is set correctly during the call
      Person.call(this, firstName);

      // Initialize our Student-specific properties
      this.subject = subject;
    };

    // Create a Student.prototype object that inherits from Person.prototype.
    // Note: A common error here is to use "new Person()" to create the
    // Student.prototype. That's incorrect for several reasons, not least
    // that we don't have anything to give Person for the "firstName"
    // argument. The correct place to call Person is above, where we call
    // it from Student.
    Student.prototype = Object.create(Person.prototype); // See note below

    // Set the "constructor" property to refer to Student
    Student.prototype.constructor = Student;

    // Replace the "sayHello" method
    Student.prototype.sayHello = function(){
      console.log("Hello, I'm " + this.firstName + ". I'm studying "
                  + this.subject + ".");
    };

    // Add a "sayGoodBye" method
    Student.prototype.sayGoodBye = function(){
      console.log("Goodbye!");
    };

    // Example usage:
    var student1 = new Student("Janet", "Applied Physics");
    student1.sayHello();   // "Hello, I'm Janet. I'm studying Applied Physics."
    student1.walk();       // "I am walking!"
    student1.sayGoodBye(); // "Goodbye!"

    // Check that instanceof works correctly
    console.log(student1 instanceof Person);  // true
    console.log(student1 instanceof Student); // true

source: https://developer.mozilla.org/en-US/docs/Web/JavaScript/Introduction_to_Object-Oriented_JavaScript

###websockets
WebSocket, unlike HTTP, provides full-duplex communication. Additionally, WebSocket enables streams of messages on top of TCP. TCP alone deals with streams of bytes with no inherent concept of a message. Before WebSocket, port 80 full-duplex communication was attainable using Comet channels; however, Comet implementation is nontrivial, and due to the TCP handshake and HTTP header overhead, it is inefficient for small messages. WebSocket protocol aims to solve these problems without compromising security assumptions of the web.

    <script language="javascript" type="text/javascript">
    var wsUri = "ws://echo.websocket.org/";
     var output;

     function init() {
        output = document.getElementById("output");
        testWebSocket();
        }

     function testWebSocket() {
        websocket = new WebSocket(wsUri);
        websocket.onopen = function(evt) { onOpen(evt) };
        websocket.onclose = function(evt) { onClose(evt) };
        websocket.onmessage = function(evt) { onMessage(evt) };
        websocket.onerror = function(evt) { onError(evt) };
        }

     function onOpen(evt) { writeToScreen("CONNECTED");
        doSend("WebSocket rocks");
        }

     function onClose(evt) {
        writeToScreen("DISCONNECTED");
        }

     function onMessage(evt) {
        writeToScreen('<span style="color: blue;">RESPONSE: ' + evt.data+'</span>');
        websocket.close();
        }

     function onError(evt) {
        writeToScreen('<span style="color: red;">ERROR:</span> ' + evt.data);
         }

     function doSend(message) {
        writeToScreen("SENT: " + message);
        websocket.send(message);
        }

     function writeToScreen(message) {
        var pre = document.createElement("p");
        pre.style.wordWrap = "break-word";
        pre.innerHTML = message; output.appendChild(pre);
        }

     window.addEventListener("load", init, false);

     </script>

     <h2>WebSocket Test</h2>

     <div id="output"></div>


sources:
http://www.websocket.org/,
http://en.wikipedia.org/wiki/WebSocket

###XMLHttpRequest

The XMLHttpRequest object is used to exchange data with a server behind the scenes.
 Such an request is demonstrated down below.

    var xhr;
    xhr = new XMLHttpRequest();
    xhr.open('GET', app.settings.ajaxUrl, true);
    xhr.addEventListener("load", app.getJSONString);
    xhr.send();
own code

###AJAX
Ajax stands for Asynchronous JavaScript and XML. AJAX allows web pages to be updated asynchronously by exchanging small amounts of data with the server behind the scenes. This means that it is possible to update parts of a web page, without reloading the whole page.
Classic web pages, (which do not use AJAX) must reload the entire page if the content should change

This is one of the simplest examples:
the Html page:

    <!DOCTYPE html>
    <html>
    <body>

    <div id="myDiv"><h2>Let AJAX change this text</h2></div>
    <button type="button" onclick="loadXMLDoc()">Change Content</button>

    </body>
    </html>

by clicking the button the browser searches the loadXMLDoc function and executes it.

    function loadXMLDoc()
    {
    .... AJAX script goes here ...
    }

The keystone of AJAX is the XMLHttpRequest object, which is explained in the previous chapter.

Sources: http://www.w3schools.com/ajax/, http://www.w3schools.com/ajax/ajax_intro.asp, http://www.w3schools.com/ajax/ajax_example.asp
###Callbacks
A callback is a reference to executable code, or a piece of executable code, that is passed as an argument to other code.

This is a very simple example:

    function myBread(param1, param2, callback) {
        alert('Started eating my bread.\n\nIt has: ' + param1 + ', ' + param2);
        callback();
    }

    myBread('ham', 'cheese', function() {
        alert('Finished eating my bread.');
    });

Here we have a function called myBread and it accepts three parameters. The third parameter is the callback function. When the function executes, it spits out an alert message with the passed values displayed. Then it executes the callback function.

Source: http://www.impressivewebs.com/callback-functions-javascript/
###How to write testable code for unit-tests
When writing testable code for unit-tests you should take in account the following:

- Write the code cohesive. Because cohesive code creates strong test fixtures and short, simple tests.
- Creating loosely coupled code because classes can more easely be tested in isolation.
- Refactoring an un-testable piece of code to something testable
- Make functions return stuff.
- Using dependency injections where it is appropriate.

source: http://tutorials.digitalprimates.net/flexunit/Unit-15.html