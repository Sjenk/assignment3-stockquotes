/*global app, data */
(function () {
    "use strict";
    window.app = {

        series: {},
        //  socket: io("http://server7.tezzt.nl:1333"),

        settings: {
            refresh: 1000,
            //  ajaxUrl: 'http://server7.tezzt.nl/~theotheu/stockquotes/index.php',
            //  ajaxUrl: 'http://server7.tezzt.nl/~theotheu/stockquotes/index.php',
            dataPoints: 10
        },


        rnd: function (min, max) {
            // generate value  between min max current value 100 so 100-min 100+max
            var r;
            if (max === undefined) {
                max = 0.4;
            }
            r = Math.random() * max;
            return (1 - max / 2 + r) * min;

        },


        parseData: function (rows) {
            var company, i;
            for (i = 0; i < rows.length; i++) {
                company = rows[i].col0;

                if (app.series[company] !== undefined) {
                    app.series[company].push(rows[i]);
                } else {
                    app.series[company] = [rows[i]];
                }
            }
            if (app.series[company].length > app.settings.dataPoints) {
                app.series[company].shift();
            }

        },

        showData: function () {
            var company, table, propertyValue, row, quote, cell, propertyName;

            //create table
            table = document.createElement("table");
            //create rows
            for (company in app.series) {

                quote = app.series[company][app.series[company].length - 1];
                row = document.createElement("tr");
                row.id = app.createValidCssNameFromCompany(company);
                if (quote.col4 > 0) {
                    row.className = "winner";
                } else if (quote.col4 < 0) {
                    row.className = "loser";
                } else {
                    row.className = "normal";
                }
                table.appendChild(row);

                //create cells
                for (propertyName in quote) {
                    propertyValue = quote[propertyName];
                    cell = document.createElement("td");
                    cell.innerText = propertyValue;
                    row.appendChild(cell);

                }
            }

            return table;
        },

        loop: function () {
            var container, table;
            container = document.querySelector("#container");

            //remove old table
            if (document.querySelector("table") !== null) {
                container.removeChild(document.querySelector("table"));
            }

            //create new table
            //  app.getDataFromAjax();
            app.generateTestData();
            table = app.showData();
            container.appendChild(table);

            setTimeout(app.loop, app.settings.refresh);
        },

        getDataFromAjax: function () {

            var xhr;
            xhr = new XMLHttpRequest();
            xhr.open('GET', app.settings.ajaxUrl, true);
            xhr.addEventListener("load", app.retrieveJSON);
            xhr.send();

        },

        retrieveJSON: function (e) {
            var str, obj, rows;
            str = e.target.responseText;
            obj = JSON.parse(str);
            rows = obj.query.results.row;
            app.parseData(rows);
        },

        getRealTimeData: function () {
            app.socket.on('stockquotes', function (data) {
                app.parseData(data.query.results.row);
            });
        },


        generateTestData: function () {
            var company, quote, newQuote, date;
            date = new Date();

            for (company in app.series) {
                quote = app.series[company][0];
                newQuote = Object.create(quote);
                newQuote.col0 = company;
                newQuote.col2 = app.getDate(date);
                newQuote.col3 = app.getTime(date);
                newQuote.col4 = app.rnd(-10, 10); //price difference between current and last value
                app.series[company].push(newQuote);
            }
        },

        getDate: function (date) {
            var datetime, month, day, year;

            month = date.getMonth()
                .toString();
            day = date.getDay()
                .toString();
            year = date.getFullYear()
                .toString();

            if (month.length < 2) {
                month = "0" + month;
            }
            if (day.length < 2) {
                day = "0" + day;
            }

            datetime = month + "/" + day + "/" + year;


            return datetime;
        },


        getTime: function (date) {
            var hours, minutes, strTime;
            hours = date.getHours()
                .toFixed();
            minutes = date.getMinutes()
                .toString();

            if (hours.length > 1) {
                if (minutes.length < 2) {
                    minutes = "0" + minutes.toString();
                }
                strTime = (24 - hours)
                    .toString() + ":" + minutes + " pm";
            } else {
                strTime = hours.toString() + ":" + minutes + " am";
            }
            return strTime;
        },

        createValidCssNameFromCompany: function (str) {
            //regular experion to remove everything.
            //that is not out of az 0-9

            return str.replace(/[^a-zA-Z_0-9]/g, "");
        },

        //doRequest : function(url){
        //    var xhr = XMLHttpRequest();
        //    xhr.open("GET", url);
        //    xhr.addEventListener("load", app.doRequests(app.a));
        //    xhr.send();
        //
        //},
        //
        //doRequests : function(arra){
        //    var url = "http://server7.tezzt.nl:1334/api/recursion/"
        //    var e;
        //   if(arra.length ===0){
        //       return;
        //   }
        //    else{
        //       e = arra.shift();
        //       url += e;
        //   }
        //    app.doRequest(url);
        //},




        initHTML: function () {
            var container, h1Node, tableNode;

            container = document.createElement("div");
            container.id = "container";

            app.container = container;

            h1Node = document.createElement("h1");
            h1Node.innerText = "Real Time Stockquote App";
            tableNode = app.showData();
            tableNode.id = "tableNode";
            app.container.appendChild(h1Node);
            app.container.appendChild(tableNode);

            return app.container;
        },

        init: function () {
            var container;
            container = app.initHTML();
            document.querySelector("body").appendChild(container);
            //app.getRealTimeData();
            app.parseData(data.query.results.row);
            app.loop();
        }
    };

}());